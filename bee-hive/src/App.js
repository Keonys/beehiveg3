import React from 'react';
import logo from './logo.svg';
import './App.css';



function App() {
    const styles = {
        width: "80%",
        height: "500px"
    };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
         I'm not like the other bee I'm a Queen !
        </p>
        <div id="chart" style={styles}></div>
        <br></br>
        <div id="camera" width="500" height="500">
        <img src="https://picsum.photos/500"></img>
        </div>
        </header>
    </div>
  );
}

export default App;
