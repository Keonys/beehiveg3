class SensorChart {

    constructor()
    {
        this.dataId = [];
        this.dataTempOutdoor = [];
        this.dataTempIndoor = [];
        this.dataHumidity = [];
        this.data = [];
        this.dataTime = [];

        this.echarts = require("echarts");
        this.myChart = this.echarts.init(document.getElementById('chart'));
        this.options = {
            title: {
                text: ''
            },
            tooltip : {
                trigger: 'axis',
                position: 'inside',
                type: "shadow"
            },
            legend: {
                data:['Id','Temp (outdoor)','Temp (indoor)','Humidity', "toto"],
                textStyle : {
                    color: "#fffff"
                }
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data:['1h','2h','3h','4h', "5h"]
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'Id',
                    type:'line',
                    stack: 'val',
                    areaStyle: {normal: {}},
                    data:[120, 132, 101, 134, 90, 230, 210]
                },
                {
                    name:'Temp (outdoor)',
                    type:'line',
                    stack: 'val',
                    areaStyle: {normal: {}},
                    data:[220, 182, 191, 234, 290, 330, 310]
                },
                {
                    name:'Temp (indoor)',
                    type:'line',
                    stack: 'val',
                    areaStyle: {normal: {}},
                    data:[150, 232, 201, 154, 190, 330, 410]
                },
                {
                    name:'Humidity',
                    type:'line',
                    stack: 'val',
                    areaStyle: {normal: {}},
                    data:[320, 332, 301, 334, 390, 330, 320]
                },
                {
                    name:'toto',
                    type:'line',
                    stack: 'val',
                    label: {
                        normal: {
                            show: true,
                            position: 'top'
                        }
                    },
                    areaStyle: {normal: {}},
                    data:[820, 932, 901, 934, 1290, 1330, 1320]
                }
            ]
        };

        return this.myChart.setOption(this.options);
    }

    updateChart(id,tempOutdoor,tempIndoor,humidity,data,time)
    {
        this.dataId.push(id);
        this.dataTempOutdoor.push(tempOutdoor);
        this.dataTempIndoor.push(tempIndoor);
        this.dataHumidity.push(humidity);
        this.data.push(data);
        this.dataTime.push(time);

        if(this.dataTime.length >= 30)
        {
            this.dataId.pop();
            this.dataTempOutdoor.pop();
            this.dataTempIndoor.pop();
            this.dataHumidity.pop();
            this.data.pop();
            this.dataTime.pop();
        }

        this.options = {
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data:this.dataTime
                }
            ],
            series: [
                {
                    data: this.dataId
                },
                {
                    data: this.dataTempOutdoor
                },
                {
                    data: this.dataTempIndoor
                },
                {

                    data: this.dataHumidity
                },
                {
                    name: 'toto',
                    type: 'line',
                    stack: 'val',
                    label: {
                        normal: {
                            show: true,
                            position: 'top'
                        }
                    },
                    areaStyle: {normal: {}},
                    data: this.data
                }
            ]
        };
        this.echarts = require("echarts");
        this.myChart = this.echarts.init(document.getElementById('chart'));
        return this.myChart.setOption(this.options);
    }
}
export default SensorChart;
