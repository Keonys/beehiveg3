import * as client from "mqtt";
import SensorChart from "./sensorChart";

class MqttController {
    constructor()
    {
         this.mqtt = require('mqtt');
         this.client  = mqtt.connect('mqtt://test.mosquitto.org');
         this.client.on('connect', function () {
            this.client.subscribe('presence', function (err) {
                if (!err) {
                    this.client.publish('presence', 'Hello mqtt');
                }
            })
        })

        this.client.on('message', function (topic, message) {
            // message is Buffer
            console.log(message.toString());
            this.client.end();
        })
    }
}
export default MqttController;
