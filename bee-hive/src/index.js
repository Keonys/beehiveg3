import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import SensorChart from './sensorChart'
import MqttController from './sensorChart'

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

var mqtt = require('mqtt');
var options = {
    port: 9001,
    host: '192.168.43.152',
    protocolId: 'MQTT',
    keepalive: 1000
};
var client  = mqtt.connect(options);

client.on('connect', function () {
    client.subscribe('presence', function (err) {
        if (!err) {
            client.publish('presence', 'Hello mqtt from toto');
        }
    })
});

client.on('message', function (topic, message) {
    console.log(message.toString());
  //  client.end();
});
var chart = new SensorChart();
serviceWorker.register('./index.js');
serviceWorker.register('./serviceWorker.js');
serviceWorker.register('./Apps.js');
